// INCLUDE:
/////////////////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>


// MACROS (for readability and inlining)
/////////////////////////////////////////////////////////////////////////////////////////////
#define IS_ALIVE(X) ((X) & 0x0FFF)		// Boolean, "is cell X alive?"

#define R_BITS(X) (((X) & 0x0F00) >> 8)	// Extract the Red colour bits from a cell
#define G_BITS(X) (((X) & 0x00F0) >> 4)	// Extract the Green colour bits from a cell
#define B_BITS(X) ((X) & 0x000F)		// Extract the Blue colour bits from a cell

// DEFINITIONS (for readability)
/////////////////////////////////////////////////////////////////////////////////////////////
#define X 8 // number of columns in board
#define Y 8 // number of rows in board

// Cell classes
#define	TOP_LEFT_CORNER 	0x1000
#define	TOP_EDGE 			0x2000 
#define	TOP_RIGHT_CORNER 	0x3000
#define	LEFT_EDGE 			0x4000 
#define	CENTRE 				0x5000 
#define	RIGHT_EDGE 			0x6000
#define	BOTTOM_LEFT_CORNER 	0x7000 
#define	BOTTOM_EDGE 		0x8000 
#define	BOTTOM_RIGHT_CORNER 0x9000


// TYPEDEFS:
/////////////////////////////////////////////////////////////////////////////////////////////
// Simple boolean type without needing stdbool.h
typedef enum {false, true} bool;

//	Datatype for 16-bit binary numbers
//		e.g.	uint16 a = 0x1234;
//
//	Cell information is stored in 16 bits
//
//	|C|C|C|C|R|R|R|R|G|G|G|G|B|B|B|B|
//
//	where:
//		C = cell class (edge, corner, centre)
//		R = red bits
//		G = green bits
//		B = blue bits
//
//	cases are defined based on cell position (i.e. cell indices)
//		1, ..., 2, ..., 3
//		.		.		.
//		.		.		.
//		.		.		.
//		4, ..., 5, ..., 6
//		.		.		.
//		.		.		.
//		.		.		.
//		7, ..., 8, ..., 9
//
typedef unsigned short int uint16;

//board data structure
typedef struct board {
	uint16 cells[Y][X];	// Board stored as rows of columns
} board;

// GLOBAL VARS:
/////////////////////////////////////////////////////////////////////////////////////////////
bool TOROID = false;


// FUNCTIONS:
/////////////////////////////////////////////////////////////////////////////////////////////

// Initialize board
void initBoard(board* b) {
	int i = 0, j = 0;

	// Iterate over rows
	for (j = 0; j < Y; ++j) {
		// Iterate over columns
		for (i = 0; i < X; ++i) {
			// TOP CASES:
			if (j == 0) {
				// Top left corner
				if (i == 0) {
					b->cells[j][i] = 0x1000;
				}
				// Top edge
				if (i > 0 && i < X-1) {
					b->cells[j][i] = 0x2000;
				}
				// Top right corner
				if (i == X-1) {
					b->cells[j][i] = 0x3000;
				}
			}

			// MIDDLE CASES:
			if (j > 0 && j < Y-1) {
				// Left edge
				if (i == 0) {
					b->cells[j][i] = 0x4000;
				}
				// Centre
				if (i > 0 && i < X-1) {
					b->cells[j][i] = 0x5000;
				}
				// Right edge
				if (i == X-1) {
					b->cells[j][i] = 0x6000;
				}
			}

			// BOTTOM CASES:
			if (j == Y-1) {
				// Bottom left corner
				if (i == 0) {
					b->cells[j][i] = 0x7000;
				}
				// Bottom Edge
				if (i > 0 && i < X-1) {
					b->cells[j][i] = 0x8000;
				}
				//Bottom right edge
				if (i == X-1) {
					b->cells[j][i] = 0x9000;
				}
			}
		}
	}
}

// Apply rules
void applyRules(board* old, board* new, int i, int j) {
	// Copy live neighbours to array, then calculate new cell values based on array entries

	// Array to store local neighbours
	uint16 moore[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	
	// Counter to track which entry in neighbours array should be copied to
	unsigned int k = 0;

	// check adjacent cells for non-zero colour data (i.e. is alive)
	uint16 type = (old->cells[j][i] &	0xF000);
	switch (type) {
		// Top left corner
		case TOP_LEFT_CORNER:
			if (IS_ALIVE(old->cells[j][i+1])) 	{moore[k] = old->cells[j][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i+1])) {moore[k] = old->cells[j+1][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i])) 	{moore[k] = old->cells[j+1][i]; 	++k;}
			
			if (TOROID) {
				if (IS_ALIVE(old->cells[Y-1][i])) 	{moore[k] = old->cells[Y-1][i]; 	++k;}
				if (IS_ALIVE(old->cells[Y-1][i+1])) {moore[k] = old->cells[Y-1][i+1]; 	++k;}
				if (IS_ALIVE(old->cells[j+1][X-1])) {moore[k] = old->cells[j+1][X-1]; 	++k;}
				if (IS_ALIVE(old->cells[j][X-1])) 	{moore[k] = old->cells[j][X-1]; 	++k;}
				if (IS_ALIVE(old->cells[Y-1][X-1]))	{moore[k] = old->cells[Y-1][X-1]; 	++k;}
			}
			break;
		// Top edge
		case TOP_EDGE:
			if (IS_ALIVE(old->cells[j][i+1])) 	{moore[k] = old->cells[j][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i+1])) {moore[k] = old->cells[j+1][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i])) 	{moore[k] = old->cells[j+1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i-1])) {moore[k] = old->cells[j+1][i-1]; 	++k;}
			if (IS_ALIVE(old->cells[j][i-1])) 	{moore[k] = old->cells[j][i-1]; 	++k;}
			
			if (TOROID) {
				if (IS_ALIVE(old->cells[Y-1][i]))	{moore[k] = old->cells[Y-1][i]; 	++k;}
				if (IS_ALIVE(old->cells[Y-1][i+1])) {moore[k] = old->cells[Y-1][i+1]; 	++k;}
				if (IS_ALIVE(old->cells[Y-1][i-1]))	{moore[k] = old->cells[Y-1][i-1]; 	++k;}
			}
			break;
		// Top right corner
		case TOP_RIGHT_CORNER:
			if (IS_ALIVE(old->cells[j+1][i])) 	{moore[k] = old->cells[j+1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i-1])) {moore[k] = old->cells[j+1][i-1]; 	++k;}
			if (IS_ALIVE(old->cells[j][i-1])) 	{moore[k] = old->cells[j][i-1]; 	++k;}
			
			if (TOROID) {
				if (IS_ALIVE(old->cells[Y-1][X-1]))	{moore[k] = old->cells[Y-1][X-1]; 	++k;}
				if (IS_ALIVE(old->cells[Y-1][0]))	{moore[k] = old->cells[Y-1][0]; 	++k;}
				if (IS_ALIVE(old->cells[j][0]))		{moore[k] = old->cells[j][0]; 		++k;}
				if (IS_ALIVE(old->cells[Y-1][i-1])) {moore[k] = old->cells[Y-1][i-1]; 	++k;}
				if (IS_ALIVE(old->cells[Y-1][i])) 	{moore[k] = old->cells[Y-1][i]; 	++k;}
			}
			break;
		// Left Edge
		case LEFT_EDGE:
			if (IS_ALIVE(old->cells[j-1][i])) 	{moore[k] = old->cells[j-1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j-1][i+1])) {moore[k] = old->cells[j-1][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j][i+1])) 	{moore[k] = old->cells[j][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i+1])) {moore[k] = old->cells[j+1][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i])) 	{moore[k] = old->cells[j+1][i]; 	++k;}
			
			if (TOROID) {
				if (IS_ALIVE(old->cells[j+1][X-1])) {moore[k] = old->cells[j+1][X-1]; 	++k;}
				if (IS_ALIVE(old->cells[j][X-1])) 	{moore[k] = old->cells[j][X-1]; 	++k;}
				if (IS_ALIVE(old->cells[j-1][X-1])) {moore[k] = old->cells[j-1][X-1]; 	++k;}
			}
			break;
		// Centre
		case CENTRE:
			if (IS_ALIVE(old->cells[j-1][i])) 	{moore[k] = old->cells[j-1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j-1][i+1])) {moore[k] = old->cells[j-1][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j][i+1])) 	{moore[k] = old->cells[j][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i+1])) {moore[k] = old->cells[j+1][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i])) 	{moore[k] = old->cells[j+1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i-1])) {moore[k] = old->cells[j+1][i-1]; 	++k;}
			if (IS_ALIVE(old->cells[j][i-1])) 	{moore[k] = old->cells[j][i-1]; 	++k;}
			if (IS_ALIVE(old->cells[j-1][i-1])) {moore[k] = old->cells[j-1][i-1]; 	++k;}
			break;
		// Right edge
		case RIGHT_EDGE:
			if (IS_ALIVE(old->cells[j-1][i])) 	{moore[k] = old->cells[j-1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i])) 	{moore[k] = old->cells[j+1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j+1][i-1])) {moore[k] = old->cells[j+1][i-1]; 	++k;}
			if (IS_ALIVE(old->cells[j][i-1])) 	{moore[k] = old->cells[j][i-1]; 	++k;}
			if (IS_ALIVE(old->cells[j-1][i-1])) {moore[k] = old->cells[j-1][i-1]; 	++k;}
			
			if (TOROID) {
				if (IS_ALIVE(old->cells[j-1][0]))	{moore[k] = old->cells[j-1][0]; ++k;}
				if (IS_ALIVE(old->cells[j][0]))		{moore[k] = old->cells[j][0]; 	++k;}
				if (IS_ALIVE(old->cells[j+1][0]))	{moore[k] = old->cells[j+1][0]; ++k;}
			}
			break;
		// Bottom left corner
		case BOTTOM_LEFT_CORNER:
			if (IS_ALIVE(old->cells[j-1][i])) 	{moore[k] = old->cells[j-1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j-1][i+1])) {moore[k] = old->cells[j-1][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j][i+1])) 	{moore[k] = old->cells[j][i+1]; 	++k;}
			
			if (TOROID) {
				if (IS_ALIVE(old->cells[0][i+1])) 	{moore[k] = old->cells[0][i+1]; 	++k;}
				if (IS_ALIVE(old->cells[0][i])) 	{moore[k] = old->cells[0][i]; 		++k;}
				if (IS_ALIVE(old->cells[0][X-1])) 	{moore[k] = old->cells[0][X-1]; 	++k;}
				if (IS_ALIVE(old->cells[j][X-1])) 	{moore[k] = old->cells[j][X-1]; 	++k;}
				if (IS_ALIVE(old->cells[j-1][X-1])) {moore[k] = old->cells[j-1][X-1]; 	++k;}
			}
			break;
		// Bottom edge
		case BOTTOM_EDGE:
			if (IS_ALIVE(old->cells[j-1][i])) 	{moore[k] = old->cells[j-1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j-1][i+1])) {moore[k] = old->cells[j-1][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j][i+1])) 	{moore[k] = old->cells[j][i+1]; 	++k;}
			if (IS_ALIVE(old->cells[j][i-1])) 	{moore[k] = old->cells[j][i-1]; 	++k;}
			if (IS_ALIVE(old->cells[j-1][i-1])) {moore[k] = old->cells[j-1][i-1]; 	++k;}

			if (TOROID) {
				if (IS_ALIVE(old->cells[0][i+1])) 	{moore[k] = old->cells[0][i+1]; ++k;}
				if (IS_ALIVE(old->cells[0][i])) 	{moore[k] = old->cells[0][i]; 	++k;}
				if (IS_ALIVE(old->cells[0][i-1])) 	{moore[k] = old->cells[0][i-1]; ++k;}
			}
			break;
		// Bottom right corner
		case BOTTOM_RIGHT_CORNER:
			if (IS_ALIVE(old->cells[j-1][i])) 	{moore[k] = old->cells[j-1][i]; 	++k;}
			if (IS_ALIVE(old->cells[j][i-1])) 	{moore[k] = old->cells[j][i-1]; 	++k;}
			if (IS_ALIVE(old->cells[j-1][i-1])) {moore[k] = old->cells[j-1][i-1]; 	++k;}
			
			if (TOROID) {
				if (IS_ALIVE(old->cells[j-1][0])) 	{moore[k] = old->cells[j-1][0]; ++k;}
				if (IS_ALIVE(old->cells[j][0])) 	{moore[k] = old->cells[j][0]; 	++k;}
				if (IS_ALIVE(old->cells[0][0])) 	{moore[k] = old->cells[0][0]; 	++k;}
				if (IS_ALIVE(old->cells[0][i])) 	{moore[k] = old->cells[0][i]; 	++k;}
				if (IS_ALIVE(old->cells[0][i-1])) 	{moore[k] = old->cells[0][i-1]; ++k;}
			}
			break;
		default:
			//Should never be here
			printf("ERROR: Executing default case for cell type (Should not happen!)\n");
			break;
	}
	
	// actions based on count
	if (IS_ALIVE(old->cells[j][i])) {
		if (k == 3 || k == 2) {
			// cell has exactly 2 or exactly 3 live neightbours, lives on
			new->cells[j][i] = old->cells[j][i];
		} else {
			// cell dies
			new->cells[j][i] = old->cells[j][i] & 0xF000;
		}
	} else {
		// Cell is dead
		if (k == 3) {
			uint16 R = (R_BITS(moore[0]) + R_BITS(moore[1]) + R_BITS(moore[2]))/3;
			if (R > 0x000F) {
				R = 0x0F00;
			} else {
				R = R << 8;
			}
			uint16 G = (G_BITS(moore[0]) + G_BITS(moore[1]) + G_BITS(moore[2]))/3;
			if (G > 0x000F) {
				G = 0x00F0;
			} else {
				G = G << 4;
			}
			uint16 B = (B_BITS(moore[0]) + B_BITS(moore[1]) + B_BITS(moore[2]))/3;
			if (B > 0x000F) {
				B = 0x000F;
			}

			// Update new board cell with R,G,B values
			new->cells[j][i] = type + R + G + B;
		} else {
			// If cell is dead and count != 3, cell stays dead
			new->cells[j][i] = old->cells[j][i] & 0xF000;
		}
	}			
}

// Advance the game one generation by evaluating the *old board and updating the *new board
void advance(board* old, board* new) {
	int i = 0, j = 0;
	for (j = 0; j < Y; ++j) {
		for (i = 0; i < X; ++i) {
			applyRules(old, new, i, j);
		}
	}	
}

void displayBoard(board* b) {
	// Display the board onto the terminal
	int i = 0, j = 0;
	for(j = 0; j < Y; ++j) {
		for(i = 0; i < X; ++i) {
			printf("%03X ", b->cells[j][i] & 0x0FFF);	
		}
		printf("\n");
	}
}

// HTML output functions
#define R 25
void outputHeader(void) {
	FILE *out;
	out = fopen("output.html", "w");
	if (out == NULL) {
		printf("ERROR: output file not opened correctly\n");
	}

   fprintf(out, "<!DOCTYPE html>\n<html>\n<head>\n<meta charset=\"UTF-8\">\n</head>\n<body>\n");
   int w;
   int h = (2*X + 1)*R; w = h;
   fprintf(out, "<canvas height=\"%d\" width=\"%d\" id=\"c\"></canvas>\n", h, w);
   fprintf(out, "<script type=\"text/javascript\">\n");
   fprintf(out, "var canvas = document.getElementById('c');\nvar ctx = canvas.getContext('2d');\n");
   fclose(out);
}

void outputBoard(board* b) {
	FILE *out = fopen("output.html", "a");
	int i, j;
	for (j = 0; j < Y; ++j) {
		for (i = 0; i < X; ++i) {
			fprintf(out, "ctx.beginPath();\n");
			fprintf(out, "ctx.arc(%d, %d, %d, 0, 2*Math.PI);\n", (2*i + 1)*R, (2*j + 1)*R, R);
			fprintf(out, "ctx.fillStyle = '#%X0%X0%X0';\n", 	R_BITS(b->cells[j][i]),
																G_BITS(b->cells[j][i]),
																B_BITS(b->cells[j][i]));
			fprintf(out, "ctx.fill();\n");
			fprintf(out, "ctx.stroke();\n");
		}
	}
	fclose(out);
}

void outputFooter(void) {
	FILE *out = fopen("output.html", "a");
	fprintf(out, "</script>\n</body>\n</html>");
	fclose(out);
}

// MAIN:
/////////////////////////////////////////////////////////////////////////////////////////////
int main(void) {

	//Testing
	board a, b;
	board* x = &a; board* y = &b;
	initBoard(x);
	initBoard(y);

	x->cells[2][1] = 0x5FFF; x->cells[2][2] = 0x5FFF; x->cells[2][3] = 0x5FFF;

	printf("Before evaluation\n");	
	displayBoard(x);
	
	printf("After 1 evaluation\n");
	advance(x, y);
	displayBoard(y);

	printf("After 2 evaluations\n");
	advance(y, x);
	displayBoard(x);

	outputHeader();
	outputBoard(x);
	outputFooter();
	return 0;
}
